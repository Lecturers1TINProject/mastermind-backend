﻿using System;
using MasterMind.Business.Models;
using MasterMind.Data.DomainClasses;
using MasterMind.Data.Repositories;
using System.Collections.Generic;
using System.Linq;
using MasterMind.Data;

namespace MasterMind.Business.Services
{
    public class WaitingRoomService : IWaitingRoomService
    {
        public WaitingRoomService(IWaitingRoomRepository waitingRoomRepository)
        {
            //TODO
        }

        public ICollection<WaitingRoom> GetAllAvailableRooms()
        {
            //TODO
            throw new NotImplementedException();
        }

        public WaitingRoom CreateRoom(WaitingRoomCreationModel roomToCreate, User creator)
        {
            //TODO
            throw new NotImplementedException();
        }

        public WaitingRoom GetRoomById(Guid id)
        {
            //TODO
            throw new NotImplementedException();
        }

        public bool TryJoinRoom(Guid roomId, User user, out string failureReason)
        {
            //TODO
            throw new NotImplementedException();
        }

        public bool TryLeaveRoom(Guid roomId, User user, out string failureReason)
        {
            //TODO
            throw new NotImplementedException();
        }
    }
}
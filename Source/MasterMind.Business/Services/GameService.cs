﻿using MasterMind.Data.DomainClasses;
using System;
using System.Linq;
using MasterMind.Data.Repositories;

namespace MasterMind.Business.Services
{
    public class GameService : IGameService
    {
        public GameService(IWaitingRoomService waitingRoomService, IGameRepository gameRepository)
        {
            //TODO
        }

        public IGame StartGameForRoom(Guid roomId)
        {
            //TODO
            throw new NotImplementedException();
        }

        public IGame GetGameById(Guid id)
        {
            //TODO
            throw new NotImplementedException();
        }

        public CanGuessResult CanGuessCode(Guid gameId, IPlayer player, int roundNumber)
        {
            //TODO
            throw new NotImplementedException();
        }

        public GuessResult GuessCode(Guid gameId, string[] colors, IPlayer player)
        {
            //TODO
            throw new NotImplementedException();
        }

        public GameStatus GetGameStatus(Guid gameId)
        {
            //TODO
            throw new NotImplementedException();
        }
    }
}